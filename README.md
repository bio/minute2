# Les activités minute
Des activités rapides pour bien prendre en main ses cours : les comprendre et les analyser pour mieux les apprendre.

- Ces activités pédagogiques sont accessibles en consultation à l'adresse https://bio.forge.apps.education.fr/minute
- Treize fiches d'activités rapide, à piocher pour faire travailler nos apprenants sur une plage temporaire contrainte
- Pratique pour proposer des activités modulaires par îlots ou par petits groupes.
- Certaines fiches peuvent également être utilisées pour lancer une activité commune à tous les apprenants d'une même classe.
- De la case de bande dessinée au podcast en passant par tout une variété d'activités permettant de mobiliser des compétences différentes et complémentaires
- Profitez-en, c'est libre !